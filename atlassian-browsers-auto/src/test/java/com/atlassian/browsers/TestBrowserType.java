package com.atlassian.browsers;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Unit tests for BrowserType functionality
 *
 * @see BrowserType
 * @since v1.1
 */
public class TestBrowserType
{

    @Test
    public void testUnknownBrowserTypeIsNull()
    {
        BrowserType type = BrowserType.typeOf("unknown-browser");
        assertNull(type);
    }

    @Test
    public void testFireFoxBrowserType()
    {
        BrowserType firefox = BrowserType.typeOf("firefox");
        assertEquals(BrowserType.FIREFOX, firefox);
    }

    @Test
    public void testFirefoxBrowserNameIsCorrect()
    {
        assertEquals("firefox", BrowserType.FIREFOX.getName());
    }

}
