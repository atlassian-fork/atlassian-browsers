package com.atlassian.browsers;

import org.junit.Test;

import java.io.File;

import static org.mockito.Mockito.verifyZeroInteractions;

/**
 * Unit tests for BrowserAutoInstaller functionality
 *
 * @see BrowserAutoInstaller
 * @since v1.1
 */
public class TestBrowserAutoInstaller
{
    private static final File targetDir = new File("target");
    private static final File testTempDir = new File(targetDir, "testTmp");

    private static class FakeUnkownBrowserConfig implements BrowserConfiguration
    {
        public File getTmpDir()
        {
            return testTempDir;
        }

        public String getBrowserName()
        {
            return "unknown-browser";
        }
    }

    @Test
    public void unknownBrowserIsNotInstalled()
    {
        final InstallConfigurator fakeConfig = FakeInstallConfigurator.spy();
        final BrowserAutoInstaller autoInstaller = new BrowserAutoInstaller(new FakeUnkownBrowserConfig(), fakeConfig);
        autoInstaller.setupBrowser();
        verifyZeroInteractions(fakeConfig);
    }
    
}
