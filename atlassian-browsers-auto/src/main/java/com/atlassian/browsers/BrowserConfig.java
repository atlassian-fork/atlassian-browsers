package com.atlassian.browsers;

import javax.annotation.Nullable;
import java.io.File;

/**
 * Contains the config for the browser such as the path to the binary to execute to start the browser
 * as well as the path to the profile for the browser.
 */
public final class BrowserConfig
{
    private final File browserPath;
    private final File binary;
    private final File profile;
    private final BrowserType browserType;

    BrowserConfig(@Nullable File browserPath, @Nullable File binary, @Nullable File profile, BrowserType browserType)    {
        this.browserPath = browserPath;
        this.binary = binary;
        this.profile = profile;
        this.browserType = browserType;
    }

    @Nullable
    public File getBrowserPath()
    {
        return browserPath;
    }

    @Nullable
    public String getBinaryPath()
    {
        return binary != null ? binary.getAbsolutePath() : null;
    }

    @Nullable
    public String getProfilePath()
    {
        if (profile != null)
        {
            return profile.getAbsolutePath();
        }

        return null;
    }

    public BrowserType getBrowserType()
    {
        return browserType;
    }
}
