package com.atlassian.browsers;

public enum BrowserType
{
    FIREFOX("firefox"),
    CHROME("chrome"),
    IE("ie"),
    OPERA("opera"),
    SAFARI("safari"),
    HTMLUNIT("htmlunit");

    private final String browseName;

    BrowserType(String name)
    {
        this.browseName = name;
    }

    public String getName()
    {
        return browseName;
    }

    public static BrowserType typeOf(String browserString)
    {
        for (BrowserType browser : BrowserType.values())
        {
            if (browserString.startsWith(browser.getName()))
            {
                return browser;
            }
        }
        return null;
    }

}
