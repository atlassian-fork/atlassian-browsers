package com.atlassian.browsers;

/**
 * An {@link InstallConfigurator} that does nothing.
 *
 * @since 2.5
 */
public class NullConfigurator extends AbstractInstallConfigurator
{

    public static final NullConfigurator INSTANCE = new NullConfigurator();
}
