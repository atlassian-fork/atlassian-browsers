#!/bin/bash

set -euo pipefail

source "$(dirname "$0")/helpers.sh"

if isHelpRequested "$*"
then
    log "$0 <driverversion> <version> <os> [--deploy]"
	log "  driverversion: valid version number of gecko driver the Firefox version. eg. v0.19.1 Check: https://github.com/mozilla/geckodriver/releases"
	log "  version: Firefox version as provided to update-browser.sh. Find your version at https://wiki.mozilla.org/Releases"
	log "  os: linux,linux64,osx,windows"
    log "  --deploy: specifies whether to do a mvn deploy:deploy-file instead of a mvn install:install-file. WARNING: this will permanently deploy given version to Maven, available to everyone"
    exit
fi

# The full version to download. eg. 1.7.3
if [[ -z ${1:-} ]]; then
    echo "You need to provide Gecko driver version"
    exit 1
fi
if [[ -z ${2:-} ]]; then
    echo "You need to provide Firefox version"
    exit 1
fi
if [[ -z ${3:-} ]]; then
    echo "You need to provide OS"
    exit 1
fi

driverVersion=$1
firefoxVersion=$2
os=$3
deploy=0

osString=$os
extString="tar.gz"
if [ $os = "linux64" ]
then
	osString="linux64"
elif [ $os = "osx" ]
then
	osString="macos"
elif [ $osString = "windows" ]
then
	osString="win64"
	extString="zip"
else
	fail "Unsupported OS: ${os}"
fi

if [[ "${4:-}" = "--deploy" ]]
then
    deploy=1

	if artifact_exists com.atlassian.browsers firefox-profile ${firefoxVersion} ${os} jar; then
    	echo "Firefox Profile ${firefoxVersion} (${os}) is already deployed. Skipping."
    	exit 0
    fi
fi

driverArchiveTmp="/tmp/geckodriver.${extString}"
url="https://github.com/mozilla/geckodriver/releases/download/${driverVersion}/geckodriver-${driverVersion}-${osString}.${extString}"
getFile "${driverArchiveTmp}" "$url"
if [ $? != 0 ]; then
    echo "Unable to download Gecko driver from $url, aborting"
    exit 1
fi

profileDir="firefox-profile-${os}"
profilePath="/tmp/${profileDir}"
mkdir -p ${profilePath}

echo "firefox profile version: ${firefoxVersion} for ${os}" > ${profilePath}/profile.package
echo "geckodriver version: ${driverVersion}" >> ${profilePath}/profile.package

qpushd /tmp

if [ $extString = "zip" ]
then
    unzip -d "${profilePath}" "${driverArchiveTmp}" || fail "Unable to unzip ${name}.zip, likely failed to download a valid Firefox driver service binary"
else
    tar -zxf "${driverArchiveTmp}" -C "${profilePath}"
fi
qpushd "${profilePath}"
zipFile "${profilePath}" "*"
profileZippedPath="${profilePath}.zip"
profileZippedFilename="${profileDir}.zip"
qpopd
log "jarFile /tmp/firefox-${firefoxVersion}-profile ${profileZippedFilename}"
jarFile /tmp/firefox-${firefoxVersion}-profile ${profileZippedFilename}
profileJarPath="/tmp/firefox-${firefoxVersion}-profile.jar"
qpopd

if [ $deploy -eq 1 ]
then
    deploy_file "${profileJarPath}" com.atlassian.browsers firefox-profile ${firefoxVersion} ${os} jar
else
    install_file "${profileJarPath}" com.atlassian.browsers firefox-profile ${firefoxVersion} ${os} jar
    log ""
    log "WARNING: This has been deployed locally to deploy to maven run with --deploy"
fi

log "Add the following to the pom.xml"
log ""
log "<dependency>"
log "  <groupId>com.atlassian.browsers</groupId>"
log "  <artifactId>firefox-profile</artifactId>"
log "  <version>${firefoxVersion}</version>"
log "  <classifier>${os}</classifier>"
log "</dependency>"

log "cleaning"
rm ${driverArchiveTmp}
rm -rf ${profilePath}
rm ${profileZippedPath}
rm ${profileJarPath}
log "cleaning done"
