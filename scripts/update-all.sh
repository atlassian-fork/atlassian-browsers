#!/usr/bin/env bash

set -euo pipefail

source "$(dirname "$0")/helpers.sh"

CWD="$(dirname "$0")"

if isHelpRequested "$*"; then
    log "$0 [--deploy]"
    log "  Reads all versions from pom.xml and invokes all other scripts"
    log "$DEPLOY_HELP_MESSAGE"
    exit
fi

deploy=""

if [[ "${1:-}" = "--deploy" ]]; then
    deploy="$1"
fi

FIREFOX_VERSION="$(get_mvn_prop firefox.version)"
${CWD}/update-browser.sh firefox windows ${FIREFOX_VERSION} ${deploy}
${CWD}/update-browser.sh firefox linux64 ${FIREFOX_VERSION} ${deploy}
${CWD}/update-browser.sh firefox osx ${FIREFOX_VERSION} ${deploy}

FIREFOX_PROFILE_VERSION="$(get_mvn_prop firefox.profile.version)"
GECKODRIVER_VERSION="$(get_mvn_prop geckodriver.version)"
${CWD}/update-firefox-profile.sh ${GECKODRIVER_VERSION} ${FIREFOX_PROFILE_VERSION} windows ${deploy}
${CWD}/update-firefox-profile.sh ${GECKODRIVER_VERSION} ${FIREFOX_PROFILE_VERSION} linux64 ${deploy}
${CWD}/update-firefox-profile.sh ${GECKODRIVER_VERSION} ${FIREFOX_PROFILE_VERSION} osx ${deploy}

CHROME_VERSION="$(get_mvn_prop chrome.version)"
${CWD}/update-browser.sh chrome windows ${CHROME_VERSION} ${deploy}
${CWD}/update-browser.sh chrome linux64 ${CHROME_VERSION} ${deploy}
${CWD}/update-browser.sh chrome osx ${CHROME_VERSION} ${deploy}

CHROME_PROFILE_VERSION="$(get_mvn_prop chrome.profile.version)"
${CWD}/update-chrome-profile.sh ${CHROME_PROFILE_VERSION} windows ${deploy}
${CWD}/update-chrome-profile.sh ${CHROME_PROFILE_VERSION} linux64 ${deploy}
${CWD}/update-chrome-profile.sh ${CHROME_PROFILE_VERSION} osx ${deploy}

IE_PROFILE_VERSION="$(get_mvn_prop ie.profile.version)"
${CWD}/update-ie-profile.sh ${IE_PROFILE_VERSION%.*} ${IE_PROFILE_VERSION#*.*.} ${deploy}
